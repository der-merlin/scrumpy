# **Scrumpy - A discord bot for asynchronous daily standups**

A relatively simple python implementation of a discord bot using discord.py. It uses asyncio to handle asynchronous calls to the discord api.

## Requirements

-   Python 3.6 or newer
-   discord.py (1.4)

## Installation

-   create a virtual env
-   install discord.py with pip
-   install PyYAML with pip
    -   Depending on the version of PyYAML and the used system libraries the bot may complain about the CLoader and the CDumper classes not being known. If this is the case edit the configuration.py module and replace the CLoader with FullLoader and remove the Dumper parameter from the yaml.load and yaml.dump call.
-   register a bot in discord
    -   enter api key into a file named 'token'
-   add bot to discord server
-   enter target-channel name into config-file
-   change user-role in config-file to only ask users with a specific role
-   enter server name into config-file

## Usage

-   open screen/tmux
-   enter virtual env
-   start standup-bot with python (`python standup_bot.py`)

## Limitations

The following limitations are known.

-   bot does not handle disconnects very well (_so a constant stable connection is necessary to avoid constantly restarting the bot_)
    -   if the bot reconnects and survives users might be asked twice (_this is caused by the ask-task is started whenever the bot goes online_)
-   bot is only tested (_and partly designed_) to work with one instance per server (_it cannot handle more than one server at the moment_)
-   some exceptions are not retrieved correctly from their tasks
-   bot cannot handle images/gifs as answers for questions (_and will subsequently crash_)
