"""
The MIT License (MIT)

Copyright (c) 2019-2020 Merlin Wasmann

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""

import discord

from discord.ext import commands
from configuration import config


def find_role_by_name(name, client):
    server = next(filter(lambda server: server.name ==
                         config.get('server'), client.guilds), None)
    return next(filter(lambda role: role.name == name, server.roles if server else []))


async def reply(ctx, title='Scrumpy obeys your command!', message=None, fields=[]):
    embed = discord.Embed(title=title, description=message)
    for field in fields:
        embed.add_field(**field)
    await ctx.send(embed=embed)


@commands.command(help='Begruesst den Nutzer.')
async def hallo(ctx):
    await reply(ctx, message='Hello Human! I also like human activities like drinking fluid.')


@commands.command(name='start', help='Fuegt den fragenden Nutzer in die Liste der zu fragenden Nutzer hinzu.')
async def add_user(ctx):
    scrumpy_role = find_role_by_name(config.get('user_role_filter'), ctx.bot)
    await ctx.message.author.add_roles(scrumpy_role)
    await reply(ctx, message='Ich werde Dich in Zukunft fragen!')


@commands.command(name='stop', help='Entfernt den fragenden Nutzer aus der Liste der zu fragenden Nutzer.')
async def remove_user(ctx):
    scrumpy_role = find_role_by_name(config.get('user_role_filter'), ctx.bot)
    await ctx.message.author.remove_roles(scrumpy_role)
    await reply(ctx, message='Ich werde Dich in Zukunft nicht mehr fragen.')


def add_commands(bot):
    bot.add_command(hallo)
    bot.add_command(add_user)
    bot.add_command(remove_user)
