"""
The MIT License (MIT)

Copyright (c) 2019-2020 Merlin Wasmann

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""

from datetime import datetime
from datetime import timedelta

from configuration import config


def get_answer_timeout():
    """Get the timeout in seconds after which a question is considered 'not answered'."""
    xmas = is_xmas(datetime.today())
    return config.get('standard').get('answer_timeout') if not xmas else config.get('special').get('answer_timeout')


def get_questions():
    """Get the list of questions to ask based on the given config."""
    xmas = is_xmas(datetime.today())
    return config.get('standard').get('questions') if not xmas else config.get('special').get('questions')


def get_reschedule_time_in_seconds():
    """Get the time to sleep in seconds until the next ask time."""
    now = datetime.today()
    start_hour, start_minute, start_second, start_microsecond = get_start_times()
    then = now.replace(hour=start_hour, minute=start_minute,
                       second=start_second, microsecond=start_microsecond)
    then += timedelta(days=1)
    delta_t = then - now
    return delta_t.seconds + 1


def get_start_times():
    """Get the start times to ask standup questions based on the given configuration."""
    xmas = is_xmas(datetime.today())
    sub_config = config.get('standard').get(
        'start') if not xmas else config.get('special').get('start')
    return sub_config.get('hour'), sub_config.get('minute'), sub_config.get('second'), sub_config.get('microsecond')


def get_greeting():
    return config.get('standard').get('greeting')


def is_xmas(now):
    """Check if given time is christmas eve."""
    return now.day == 24 and now.month == 12


def days_in_a_row(days, amount=3):
    """Check if amount or more days are consecutive days."""
    for day0, day1 in zip(days[:-1], days[1:]):
        if (day0 - day1).days == 1:
            amount -= 1
        else:
            break
    return amount <= 1


def log(msg):
    """Print the given message and prepend the current datetime."""
    print(f'{datetime.now().isoformat()[0:-7]}: {msg}')
