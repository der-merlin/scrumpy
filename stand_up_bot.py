"""
The MIT License (MIT)

Copyright (c) 2019-2020 Merlin Wasmann

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""

import asyncio
import discord
import traceback
import yaml

import silent_users
import utility

from datetime import datetime
from discord.ext import tasks, commands

from commands import add_commands
from configuration import config

try:
    with open('token') as token_file:
        TOKEN = token_file.readline()
except FileNotFoundError:
    utility.log(
        'Unable to find "token" file. Please create a file named "token" containing your bots API key.')
    exit(1)
utility.log(f'started with config\n{config}')

silent_users.init_db()
# client = discord.Client()
client = commands.Bot(command_prefix='!')


@client.event
async def on_ready():
    utility.log(f'We have logged in as {str(client.user)}')


def find_channel_by_name(name, channels):
    return next(filter(lambda channel: channel.name == name, channels), None)


def filter_user(user):
    if user == client.user:
        return False

    if config.get('user_role_filter') not in map(str, user.roles):
        return False

    return True


def get_users():
    return set(filter(filter_user, client.get_all_members()))


@tasks.loop(hours=24.0)
async def process_users():
    """Process all members provided by get_users."""
    try:
        target_channel = find_channel_by_name(
            config.get('target_channel'), client.get_all_channels())
        for user in get_users():
            utility.log(f'now asking {user.display_name}')
            client.loop.create_task(process_user(user, target_channel))
    except Exception as ex:
        utility.log(f'exception while processing users: {ex}')
        traceback.print_exc()


@process_users.before_loop
async def before_process_users():
    utility.log('waiting for ready')
    await client.wait_until_ready()
    seconds = utility.get_reschedule_time_in_seconds()
    utility.log(f'sleeping for {str(seconds)}')
    await asyncio.sleep(seconds)


async def process_user(user, target_channel):
    """Process the given user by asking questions and collecting answers and then posting the answers in the given 
    target_channel.

    First send a private message to the user with a greeting. If this fails the user is considered 'silent' and 
    his questions are not considered answered. After that collect the answers for the configured questions, create 
    answer embeds based on them and post them into the target_channel.

    Keyword arguments:
        user (discord.Member): the object that represents the user to process
        target_channel (discord.TextChannel): the object that represents the target channel answers are posted to
    """
    try:
        await user.send(content=utility.get_greeting())
        answers, silent = await get_answers(user)
    except discord.errors.Forbidden:
        answers = {question: config.get('no_answer_msg')
                   for question in utility.get_questions()}
        silent = True

    try:
        embed = create_user_answer_output(user, answers)
        await target_channel.send(embed=embed)

        if silent and silent_users.handle_user(user, datetime.now(), days_in_row=config.get('punish_days_number')):
            await target_channel.send(content=user.mention + config.get('punish_msg'))
    except Exception as ex:
        utility.log(f'exception while processing user {user.name}: {ex}')
        traceback.print_exc()


async def get_answers(user):
    """Get answers for the user.

    Keyword arguments:
        user (discord.Member): the user to get answers for

    Returns:
        dict: answers dictionary from questions to user's answers
        bool: flag denoting if the user has been silent for one or more questions
    """
    answers = dict()
    timeout = utility.get_answer_timeout()
    questions = utility.get_questions()
    silent = False

    for question in questions:
        start = datetime.now()
        answer, not_answered = await get_answer(user, question, timeout)
        timeout = timeout - (datetime.now() - start).seconds
        answers[question] = answer
        silent |= not_answered

    return answers, silent


async def get_answer(user, question, timeout):
    """Get the answer for the given question by the given user in the given timout.

    Keyword Arguments:
        user (discord.Member):
        question (str): the question string to answer
        timeout (int): the answer time for the user (in seconds)

    Returns:
        str: the answer text entered by the user
        bool: flag denoting if the user has not answered the question within the timeout
    """
    def check_message(message):
        return message.channel == user.dm_channel and message.author == user

    answer = None
    silent = False
    # ask the question
    await user.send(content=question)
    # wait for the answer
    try:
        answer = await client.wait_for('message', timeout=timeout, check=check_message)
    except asyncio.TimeoutError:
        # this error is thrown whenever the timeout is reached
        pass

    if answer is not None:
        answer_text = answer.content
    else:
        answer_text = config.get('no_answer_msg')
        silent = True

    return answer_text, silent


def create_user_answer_output(user, answers):
    """Create the post for the standup channel based on the given answers for the given user.

    Keyword arguments:
        user (discord.Member): the user to create the output for
        answers (dict): the answers dictionary to base the output on

    Returns:
        discord.Embed: the embed object to represent the answer output
    """
    questions = utility.get_questions()
    embed = discord.Embed(title=user.display_name + ' hat geantwortet!')
    for question in questions:
        embed.add_field(name=question, value=answers[question], inline=False)
    return embed


@client.event
async def on_member_join(user):
    utility.log(f'{str(user)} joined the server')
    await user.send("Herzlich willkommen! Bitte gebe `!help` fuer Informationen zu den Verfuegbaren Bot-Commands ein.")


def unretrieved_exception_handler(loop, context):
    """Handle unretrieved exceptions by logging them."""
    utility.log(
        f'Caught unretrieved exception with message: {str(context.get("exception", "No exception found"))} {context["message"]}')


add_commands(client)

process_users.start()

client.loop.set_exception_handler(unretrieved_exception_handler)
client.run(TOKEN)
