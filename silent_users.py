"""
The MIT License (MIT)

Copyright (c) 2019-2020 Merlin Wasmann

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
"""

import sqlite3

import utility

DB_FILE = 'silent_users.db'


def init_db():
    try:
        conn = sqlite3.connect(DB_FILE, detect_types=sqlite3.PARSE_DECLTYPES |
                               sqlite3.PARSE_COLNAMES)
        cursor = conn.cursor()
        cursor.execute(
            'CREATE TABLE IF NOT EXISTS silent (id integer primary key autoincrement, user text, date date)')
        conn.commit()
    except sqlite3.Error as error:
        utility.log(str(error))
    finally:
        if conn:
            conn.close()


def handle_user(user, date, days_in_row):
    try:
        conn = sqlite3.connect(DB_FILE, detect_types=sqlite3.PARSE_DECLTYPES |
                               sqlite3.PARSE_COLNAMES)
        cursor = conn.cursor()
        cursor.execute(
            'INSERT INTO silent (user, date) VALUES (?, ?)', (user.name, date))
        conn.commit()
        cursor.execute(
            "SELECT COUNT(*) FROM silent WHERE user = ? AND date > (SELECT DATETIME('now', '-{} day'))".format(days_in_row-1), (user.name,))
        result = cursor.fetchone()
    except sqlite3.Error as error:
        utility.log(str(error))
    finally:
        if conn:
            conn.close()

    return result[0] >= days_in_row
